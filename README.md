# Custom Drawing with Canvas!

This project is a demo project for some talks i've given. 

You can find the slides for those talks here: 
* https://speakerdeck.com/jlamson/custom-drawing-with-canvas
* https://speakerdeck.com/jlamson/kotlinize-your-canvas
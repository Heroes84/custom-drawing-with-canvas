package com.darkmoose117.canvas.barchart

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import android.view.animation.AccelerateInterpolator

import com.darkmoose117.canvas.R
import com.darkmoose117.canvas.extensions.getColorCompat
import com.darkmoose117.canvas.extensions.random
import com.darkmoose117.canvas.extensions.withStyledAttributes
import java.util.*

class BarChartView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0)
: View(context, attrs, defStyleAttr) {

    // Local Data
    private var dummyData: Array<DummyData> = generateDummyData()

    // Paints
    private val barPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.FILL
        color = context.getColorCompat(R.color.bar_color)
    }
    private val axisPaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = context.getColorCompat(R.color.grid_color)
        strokeWidth = resources.getDimensionPixelSize(R.dimen.bar_chart_grid_thickness).toFloat()
    }
    private val gridLinePaint: Paint = Paint(Paint.ANTI_ALIAS_FLAG).apply {
        style = Paint.Style.STROKE
        color = context.getColorCompat(R.color.guide_color)
        strokeWidth = resources.getDimensionPixelSize(R.dimen.bar_chart_guide_thickness).toFloat()
    }

    // Dimens
    private var columnSpacing: Float = 0f
    private var padding: Float = 0f
    private var animatingFraction: Float = 1.toFloat()

    // Re-usable to avoid
    private val grid = RectF()

    // Animator
    private val animator = ValueAnimator().apply {
        interpolator = AccelerateInterpolator()
        duration = 500
        addUpdateListener { animator ->
            // Get our float from the animation. This method returns the Interpolated float.
            animatingFraction = animator.animatedFraction

            // MUST CALL THIS to ensure View re-draws;
            invalidate()
        }
    }


    init {
        context.withStyledAttributes(attrs, R.styleable.BarChartView) {
            columnSpacing = getDimensionPixelOffset(
                    R.styleable.BarChartView_android_spacing,
                    Math.round(2 * resources.displayMetrics.density)).toFloat()
            padding = getDimensionPixelOffset(
                    R.styleable.BarChartView_android_padding, 0).toFloat()
        }
    }

    fun animateNewData() {
        dummyData = generateDummyData()
        animator.apply {
            setFloatValues(0f, 1f)
            start()
        }
    }

    private fun generateDummyData(): Array<DummyData> {
        return Array((5..15).random()) {
            DummyData(Random().nextFloat())
        }
    }

    override fun onDraw(canvas: Canvas) {
        grid.set(padding, padding, width - padding, height - padding)

        canvas.apply {
            drawHorizontalGridLines(
                    numberOfGridLines = 10,
                    left = grid.left,
                    right = grid.right,
                    paint = gridLinePaint) { index ->
                val gridSpacing = grid.height() / 10f
                grid.top + index * gridSpacing
            }
            drawEvenlySpacedBars(
                    inputData = dummyData,
                    gridBounds = grid,
                    columnSpacing = columnSpacing,
                    paint = barPaint) {
                it.value * animatingFraction
            }
            drawBottomLeftAxis(
                    gridBounds = grid,
                    paint = axisPaint)
        }

    }

    private fun Canvas.drawBottomLeftAxis(gridBounds: RectF, paint: Paint) {
        drawLine(gridBounds.left, gridBounds.bottom, gridBounds.left, gridBounds.top, paint)
        drawLine(gridBounds.left, gridBounds.bottom, gridBounds.right, gridBounds.bottom, paint)
    }


    private inline fun Canvas.drawHorizontalGridLines(numberOfGridLines: Int,
                                                      left: Float, right: Float,
                                                      paint: Paint,
                                                      heightForGridLineIndex: (Int) -> Float) {
        var y: Float
        for (i in 0 until numberOfGridLines) {
            y = heightForGridLineIndex(i)
            drawLine(left, y, right, y, paint)
        }
    }

    private inline fun <T> Canvas.drawEvenlySpacedBars(inputData: Array<T>,
                                                       gridBounds: RectF,
                                                       columnSpacing: Float = 0f,
                                                       paint: Paint,
                                                       fractionHeightForData: (T) -> Float) {
        val totalHorizontalSpacing = columnSpacing * (inputData.size + 1)
        val barWidth = (gridBounds.width() - totalHorizontalSpacing) / inputData.size
        var barLeft = gridBounds.left + columnSpacing
        var barRight = barLeft + barWidth
        for (datum in inputData) {
            // Figure out top of column based on INVERSE of percentage. Bigger the percentage, the
            // smaller top is, since 100% goes to 0.
            val top = gridBounds.top + gridBounds.height() * (1f - fractionHeightForData(datum))
            drawRect(barLeft, top, barRight, grid.bottom, paint)

            // Shift over left/right column bounds
            barLeft += columnSpacing + barWidth
            barRight += columnSpacing + barWidth
        }
    }
}

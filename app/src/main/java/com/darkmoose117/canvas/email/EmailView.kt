package com.darkmoose117.canvas.email

import android.content.Context
import android.graphics.Canvas
import android.graphics.Typeface
import android.os.Build
import android.text.Layout
import android.text.SpannableString
import android.text.StaticLayout
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.StyleSpan
import android.util.AttributeSet
import android.view.View

import com.darkmoose117.canvas.R
import com.darkmoose117.canvas.extensions.*
import kotlin.math.min

class EmailView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0)
: View(context, attrs, defStyleAttr) {

    init {
        initDimens()
        initPaints()
    }

    private var email: Email? = null
    private var iconDrawable: LetterDrawable = newLetterDrawable('?', sIconSize)
    private var fromLayout: StaticLayout? = null
    private var previewLayout: StaticLayout? = null

    private val textWidth
        get() = (width
            - 2 * sPadding - sIconSize // content and padding to left
            - sPadding) // content and padding on right


    fun setEmail(email: Email) {
        this.email = email
        if (width != 0 && height != 0) {
            setupLayout()
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (email != null) {
            setupLayout()
        }
    }

    private fun setupLayout() {
        // Create From String
        email?.let {
            iconDrawable = newLetterDrawable(it.people[0][0], sIconSize, sPadding)
        }

        val from = email?.people?.let {
            it.joinToString(separator = ", ") { it }
        } ?: ""

        // Create From Layout
        // Note that commaEllipsize has been replaced by "TextUtils.listEllipsize" in 'O'. It handles
        // Internationalization as well as fixes several bugs with commaEllipsize.
        val ellipsizedFrom = TextUtils.commaEllipsize(from, sFromPaint,
                textWidth.toFloat(), "+1", "+%d")
        fromLayout = StaticLayout(ellipsizedFrom, sFromPaint,
                textWidth, Layout.Alignment.ALIGN_NORMAL, 1f, 1f, false)

        // setup previewLayout with full preview texts (any number of lines)
        val preview = "${email?.subject} \u2014 ${email?.body}"
        previewLayout = getPreviewLayout(textWidth, preview)

        // If we're below api 23, we need to manually ellipsize.
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            // find the index of the first letter of the third line
            val twoLineIndex =
                    previewLayout?.getLineStart(Math.min(previewLayout?.lineCount ?: 2, 2)) ?:
                    preview.length

            // if the index is less than the entire string, chop of the end and add '...'
            if (twoLineIndex < preview.length) {
                previewLayout = getPreviewLayout(textWidth, preview.subSequence(0, twoLineIndex - 3).toString() + "\u2026")
            }
        }
    }

    private fun getPreviewLayout(textWidth: Int, text: String): StaticLayout {
        // make subject bold
        val preview = SpannableString(text)
        preview[0..min(text.length, email?.subject?.length ?: 0)] = StyleSpan(Typeface.BOLD)

        // create layout with full text
        return createStaticLayout(source = preview,
                    paint = sPreviewPaint,
                    textWidth = textWidth)
    }

    override fun onMeasure(widthMeasureSpec: Int, ignored: Int) {
        val heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(
                resources.getDimensionPixelSize(R.dimen.email_item_height),
                View.MeasureSpec.EXACTLY)

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)

        iconDrawable.draw(canvas)

        canvas.withTranslation((2 * sPadding + sIconSize).toFloat(), sPadding.toFloat()) {
            fromLayout?.draw(canvas)
            canvas.withTranslation(0f, sFromPaint!!.textSize + 2) {
                previewLayout?.draw(canvas)
            }
        }
    }

    private fun newLetterDrawable(letter: Char, size: Int, padding: Int = 0) : LetterDrawable {
        return LetterDrawable(context, letter).apply {
            setBoundsToSizeWithPadding(size, padding)
        }
    }

    private fun initDimens() {
        if (sPadding < 0) sPadding = resources.getDimensionPixelSize(R.dimen.email_padding)
        if (sIconSize < 0) sIconSize = resources.getDimensionPixelSize(R.dimen.email_icon_size)
    }

    private fun initPaints() {
        sFromPaint = sFromPaint ?: TextPaint().apply {
            isAntiAlias = true
            color = context.getColorCompat(R.color.subject_text_color)
            textSize = resources.getDimensionPixelSize(R.dimen.email_from_text_size).toFloat()
        }

        sPreviewPaint = sPreviewPaint ?: TextPaint().apply {
            isAntiAlias = true
            color = context.getColorCompat(R.color.subject_text_color)
            textSize = resources.getDimensionPixelSize(R.dimen.email_preview_text_size).toFloat()
        }
    }

    companion object {
        private var sPadding = -1
        private var sIconSize = -1
        private var sFromPaint: TextPaint? = null
        private var sPreviewPaint: TextPaint? = null
    }
}

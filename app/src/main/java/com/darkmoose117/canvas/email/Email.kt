package com.darkmoose117.canvas.email

data class Email(val people: List<String>,
                 val subject: String,
                 val body: String,
                 val numberOfReplies: Int,
                 val isFavorite: Boolean,
                 val hasAttachment: Boolean,
                 val time: Long)

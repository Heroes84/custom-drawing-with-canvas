package com.darkmoose117.canvas.email

import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

import com.darkmoose117.canvas.DemoActivity
import com.darkmoose117.canvas.R
import kotlinx.android.synthetic.main.activity_email.*

class EmailActivity : DemoActivity() {

    override val titleResId: Int = R.string.email_demo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_email)

        recycler_view.apply {
            layoutManager = LinearLayoutManager(this@EmailActivity,
                    LinearLayoutManager.VERTICAL,
                    false)
            adapter = EmailAdapter(EmailDataHelper.getDemoEmailList())
            // NOTE RecyclerView ItemDecorations ALSO have an onDraw.
            // Check out the DividerItemDecoration source for a great example.
            addItemDecoration(DividerItemDecoration(this@EmailActivity,
                    DividerItemDecoration.VERTICAL))
        }
    }

    private class EmailAdapter(val emails: List<Email>) : RecyclerView.Adapter<EmailViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmailViewHolder {
            return EmailViewHolder(EmailView(parent.context))
        }

        override fun onBindViewHolder(holder: EmailViewHolder, position: Int) {
            holder.emailView.setEmail(emails[position])
        }

        override fun getItemCount(): Int {
            return emails.size
        }

    }

    private class EmailViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val emailView: EmailView = itemView as EmailView
    }
}

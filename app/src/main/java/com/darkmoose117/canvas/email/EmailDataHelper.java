package com.darkmoose117.canvas.email;

import android.text.format.DateUtils;

import java.util.Arrays;
import java.util.List;

class EmailDataHelper {

    private static final String[][] sPeople = {
            { "Bob" },
            { "Jim", "Alice" },
            { "Dude", "Sam", "Doug", "Jamie", "Anj" },
            { "Morty" },
            { "Marky", "Ricky", "Danny", "Terry", "Mikey", "Davey", "Timmy", "Tommy", "Joey", "Robby", "Johnny", "Brian"},
    };

    private static final String[] sSubjects = {
            "Sup!",
            "Time off",
            "[project] Name of Branch",
            "[project] Really long subject. No I mean like reeeeeeally long. Like, unreasonably long. Who would even do this?",
            "Fw: Fw: Fw: Re: Fw: This story will warm your heart!! <3 <3 <3",
    };

    private static final String sLongBody =
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non finibus dolor, sit " +
                    "amet porttitor lacus. Nullam id vehicula ipsum. Curabitur non sodales " +
                    "ligula. Pellentesque sed pulvinar magna, at tempor nulla. Sed mattis " +
                    "consequat ligula, sit amet luctus dolor maximus id. Mauris scelerisque, " +
                    "justo ac pharetra maximus, quam est tincidunt leo, in rutrum neque augue non" +
                    " est. Vivamus lacinia augue at massa elementum, auctor commodo urna mattis. " +
                    "Ut sed rhoncus risus, sed condimentum purus.";

    private static String sShortBody = "How're you?";

    private static final int[] sNumberOfReplies = {
            1,
            3,
            17,
            2,
            1
    };

    private static final byte[] sIsFavorites = { 0, 1, 0, 0, 1 };

    private static final byte[] sHasAttachments = { 1, 0, 1, 0, 1 };

    private static final long[] sTimes = {
            System.currentTimeMillis() - 5 * DateUtils.MINUTE_IN_MILLIS,
            System.currentTimeMillis() - 20 * DateUtils.MINUTE_IN_MILLIS,
            System.currentTimeMillis() - 45 * DateUtils.MINUTE_IN_MILLIS,
            System.currentTimeMillis() - DateUtils.HOUR_IN_MILLIS,
            System.currentTimeMillis() - 6 * DateUtils.HOUR_IN_MILLIS,
            System.currentTimeMillis() - 12 * DateUtils.HOUR_IN_MILLIS,
            System.currentTimeMillis() - DateUtils.DAY_IN_MILLIS,
            System.currentTimeMillis() - 3 * DateUtils.DAY_IN_MILLIS,
            System.currentTimeMillis() - 5 * DateUtils.DAY_IN_MILLIS,
            System.currentTimeMillis() - DateUtils.WEEK_IN_MILLIS
    };

    private static final Email[] EMAILS = {
            new Email(Arrays.asList(sPeople[0]), sSubjects[0], sLongBody, sNumberOfReplies[0],
                    sIsFavorites[0] == 1, sHasAttachments[0] == 1, sTimes[0]),
            new Email(Arrays.asList(sPeople[1]), sSubjects[1], sShortBody, sNumberOfReplies[1],
                    sIsFavorites[1] == 1, sHasAttachments[1] == 1, sTimes[1]),
            new Email(Arrays.asList(sPeople[2]), sSubjects[2], sLongBody, sNumberOfReplies[2],
                    sIsFavorites[2] == 1, sHasAttachments[2] == 1, sTimes[2]),
            new Email(Arrays.asList(sPeople[3]), sSubjects[3], sShortBody, sNumberOfReplies[3],
                    sIsFavorites[3] == 1, sHasAttachments[3] == 1, sTimes[3]),
            new Email(Arrays.asList(sPeople[4]), sSubjects[4], sLongBody, sNumberOfReplies[4],
                    sIsFavorites[4] == 1, sHasAttachments[4] == 1, sTimes[4]),
            new Email(Arrays.asList(sPeople[0]), sSubjects[0], sShortBody, sNumberOfReplies[0],
                    sIsFavorites[0] == 1, sHasAttachments[0] == 1, sTimes[5]),
            new Email(Arrays.asList(sPeople[1]), sSubjects[1], sLongBody, sNumberOfReplies[1],
                    sIsFavorites[1] == 1, sHasAttachments[1] == 1, sTimes[6]),
            new Email(Arrays.asList(sPeople[2]), sSubjects[2], sShortBody, sNumberOfReplies[2],
                    sIsFavorites[2] == 1, sHasAttachments[2] == 1, sTimes[7]),
            new Email(Arrays.asList(sPeople[3]), sSubjects[3], sLongBody, sNumberOfReplies[3],
                    sIsFavorites[3] == 1, sHasAttachments[3] == 1, sTimes[8]),
            new Email(Arrays.asList(sPeople[4]), sSubjects[4], sShortBody, sNumberOfReplies[4],
                    sIsFavorites[4] == 1, sHasAttachments[4] == 1, sTimes[9])
    };

    static List<Email> getDemoEmailList() {
        return Arrays.asList(EMAILS);
    }
}

package com.darkmoose117.canvas.touch

import android.os.Bundle

import com.darkmoose117.canvas.DemoActivity
import com.darkmoose117.canvas.R

class TouchTrackingActivity : DemoActivity() {

    override val titleResId: Int = R.string.touch_tracking_demo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_touch)
    }
}

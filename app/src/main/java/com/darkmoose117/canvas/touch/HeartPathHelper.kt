package com.darkmoose117.canvas.touch

import android.graphics.Matrix
import android.graphics.Path

object HeartPathHelper {

    /**
     * @return a Path of a Heart that fit's into a 1x1 box. All points are floats from 0 -> 1 to
     * allow for easy scaling to the desired size.
     */
    private val heartPath: Path
        get() = Path().apply {
            moveTo(1f / 2, 1f / 5)
            cubicTo(5f / 14, 0f,
                    0f, 1f / 15,
                    1f / 28, 2f / 5)
            cubicTo(1f / 14, 2f / 3,
                    3f / 7, 5f / 6,
                    1f / 2, 1f)
            cubicTo(4f / 7, 5f / 6,
                    13f / 14, 2f / 3,
                    27f / 28, 2f / 5)
            cubicTo(1f, 1f / 15,
                    9f / 14, 0f,
                    1f / 2, 1f / 5)
        }

    /**
     *
     * @param size the size square to draw the Heart in.
     * @return [.getHeartPath] scaled to the provided size.
     */
    fun getHeartOfSize(size: Int): Path {
        return heartPath.apply {
            transform(uniformScaleMatrix(size.toFloat()))
        }
    }

    /**
     * Creates a scale matrix with the scale factor [scale] on both the 'x' and 'y' axis
     */
    fun uniformScaleMatrix(scale: Float = 1.0f) = Matrix().apply { setScale(scale, scale) }

}
